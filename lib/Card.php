<?
namespace Bestgarden\Gds\MobileApi;

Class CardIterator
    extends Api
{
	private $modul = 'gds.mobileapi';
	private $option = 'card_iterator';
	public $number = '';
	
	function __construct(){
		global $DB;
		$DB->StartTransaction();
		
		$iterator = intval(\Bitrix\Main\Config\Option::get($this->modul, $this->option));
		\Bitrix\Main\Config\Option::set($this->modul, $this->option, ++ $iterator);
		
		$DB->Commit();
		
		$this->number = $this->format($iterator);
	}
	
	function format($number){
		$zero = '0000000';
		return substr($zero, strlen($number)).$number;
	}
	
	function current(){
		$iterator = \Bitrix\Main\Config\Option::get($this->modul, $this->option);
		if($iterator){
			return $this->format($iterator);
		}
	}
	
}