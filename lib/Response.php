<?
namespace Bestgarden\Gds\MobileApi;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Response
{
	static private $status = '';
	static private $message = '';
	static private $errorCode = '0';
	static private $lang = 'RU';

	public function succes($message, $params){

		self::$status = 'ok';

		if(!is_array($message)){
			if(is_array($params) && !empty($params)){
			$paramsTmp = array();
			foreach($params as $key => $param){
				$paramsTmp['#'.$key.'#'] = $param;
			}

			$params = $paramsTmp;
			unset($paramsTmp);
			}
			$message = GetMessage($message, $params);
		}else{
			$message = self::convertArray($message);
		}

		self::$message = $message;
		self::show();
	}

	public function error($code, $params = array()){
		if(is_array($params) && !empty($params)){
			$paramsTmp = array();
			foreach($params as $key => $param){
				$paramsTmp['#'.$key.'#'] = $param;
			}

			$params = $paramsTmp;
			unset($paramsTmp);
		}
		self::$status = 'error';
		self::$message = GetMessage($code, $params);
		self::$errorCode = $code;
		self::show();
	}

	static private function show(){
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		$result = array(
			'status' => self::$status,
			'message' => self::$message,
			'error_code' => self::$errorCode
		);

		if($_GET['type'] == 'array'){
			echo '<pre>'.print_r($result ,true).'</pre>';
		}else{
			echo json_encode($result);
		}
		//header('Content-type: text/html; charset=utf-8');
		die();
	}

	static function setLang($lang){
		$lang = ToLower($lang);

		self::$lang = $lang;
		Bitrix\Main\Localization\Loc::setCurrentLang($lang);
		Bitrix\Main\Localization\Loc::loadLanguageFile(__DIR__.'/../index.php');
	}

	static function setDefaultLang(){
		self::setLang(self::$lang);
	}

	public function convertArray(&$value){
		if(is_array($value)){
			foreach($value as $k => &$v){
				self::convertArray($v);
			}
		}else{
			$value = iconv ('cp1251', 'UTF-8', $value);
		}
		return $value;
	}
}
