<?
namespace Bestgarden\Gds\MobileApi;

class Mark extends Api {
	protected $iblockMarksId = 15;
	
	public function __construct() {
	   parent::__construct();
	   \Bitrix\Main\Loader::includeModule('iblock');
	}
	
	public function add() {
		$this->addCheckFields(array("CARD", "MARK", "SHOPCODE"));
		$this->checkFields();
		$arData = $this->getRequest();
		$shop = new Shop;
		$shop = $shop->getByCode($arData["SHOPCODE"]);
		$this->CheckHash($this->GetPassword("shop", $shop["ID"]));
		
		$user = new User;
		$userId = $user->getIdByCard($arData["CARD"]);
		$element = new \CIBlockElement;
		$arFields = array(
			"IBLOCK_ID" => $this->iblockMarksId,
			"NAME" => $this->encode($arData["MARK"]),
			"PROPERTY_VALUES" => array(
				"CLIENT" => $userId,
				"SHOP" => $shop["ID"]
			)
		);
		if(!$elementId = $element->Add($arFields)) {
			$this->setException("ERROR_MARK_ADD");
		}
		$this->setResult("OK_MARK_ADD");
	}
	
	public function shoplist() {
		$this->addCheckFields(array("SHOPCODE"));
		$this->checkFields();
		$arData = $this->getRequest();
		$shop = new Shop;
		$shop = $shop->getByCode($arData["SHOPCODE"]);
		$this->CheckHash($this->GetPassword("shop", $shop["ID"]));
		
		$arFilter = array(
			"IBLOCK_ID" => $this->iblockMarksId,
			"PROPERTY_SHOP" => $shop["ID"]
		);
		if (strlen($arData["EMAIL"]) > 0) {
			$user = new User;
			$arFilter["PROPERTY_CLIENT"] = $user->getIdByEmail($arData["EMAIL"]);
		}
		$dbResult = \CIblockElement::GetList(array(), $arFilter, false, false, array("IBLOCK_ID", "ID", "NAME"));
		$arList = array();
		while ($mark = $dbResult->Fetch()) {
			if(!in_array($mark["NAME"], $arList)) {
				$arList[] = $mark["NAME"];
			}
		}
		$this->setResult($arList);
	}
}
?>
