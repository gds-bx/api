<?
namespace Bestgarden\Gds\MobileApi;

class Cleaner {
	public $user = array();
	private $email = "lmi@greenkevich.by";
	private $arIBlocks = array(
		"MARK" => 15,
		"REVIEW" => 14,
		"TRANSACTION" => 8,
		"SHOP" => 9
	);

	public function __construct($numberCard) {
		\Bitrix\Main\Loader::includeModule('iblock');
		$this->user = $this->getUser($numberCard);
	}
	
	public function __destruct() {
		//\CUser::Delete($this->user['ID']);
	}

	public function cleanData() {
		if ($this->user['LOGIN'] == $this->email) {
			$this->removeReviews();
			$this->removeMarks();
			$this->removeTransactions();
			$this->removeShops();
			return 1;
		}
		return 0;
	}
	
	public function getUser($numberCard) {
		$res = \CUser::GetList(($by = "LAST_NAME"), ($order = "asc"), array('UF_NUMBER_CART' => $numberCard));
		if($user = $res->fetch()) 
			return $user;
		return array();
	}

	private function removeShops() {
		$arFilter = array('PROPERTY_SHOP_ADMIN' => $this->user['ID'], 'IBLOCK_ID' => $this->arIBlocks['SHOP']);
		$arShopID = $this->getIdArray($arFilter);
		if (!empty($arShopID))
			$this->remove($arShopID);
	}

	private function removeTransactions() {
		$arFilter = array('PROPERTY_BUYER_MAIL' => $this->user['LOGIN'], 'IBLOCK_ID' => $this->arIBlocks['TRANSACTION']);
		$arTransactionID = $this->getIdArray($arFilter);
		if (!empty($arTransactionID))
			$this->remove($arTransactionID);
	}

	private function removeReviews() {
		$arFilter = array('PROPERTY_CLIENT' => $this->user['ID'], 'IBLOCK_ID' => $this->arIBlocks['REVIEW']);
		$arReviewID = $this->getIdArray($arFilter);
		if (!empty($arReviewID))
			$this->remove($arReviewID);
	}

	private function removeMarks() {
		$arFilter = array('PROPERTY_CLIENT' => $this->user['ID'], 'IBLOCK_ID' => $this->arIBlocks['MARK']);
		$arMarkID = $this->getIdArray($arFilter);
		if (!empty($arMarkID))
			$this->remove($arMarkID);
	}

	private function getIdArray($arFilter) {
		$arResult = array();
		$res = \CIBlockElement::GetList(array(), $arFilter, false, false, array('ID'));
		while($ob = $res->fetch()) {
			$arResult[] = $ob['ID'];
		}
		return $arResult;
	}

	private function remove($arID) {
		foreach($arID as $id)
			\CIBlockElement::Delete($id);
	}
}