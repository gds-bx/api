<?
namespace Bestgarden\Gds\MobileApi;

Class ExceptionApi
    extends \Exception
{
    protected $params = array();

    public function __construct($message = "", $code = 0, \Exception $previous = null, $params = array())
    {
        if(is_array($params))
            $this->params = $params;

        parent::__construct($message, $code, $previous);
    }

    public function getParams(){
        return $this->params;
    }
}
