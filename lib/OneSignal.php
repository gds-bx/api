<?
namespace Bestgarden\Gds\MobileApi;

class OneSignal extends Api {
	protected $iblockAppsId = 17;
	protected $iblockProvidersId = 18;
	protected $iblockSubsId = 20;
	protected $iblockMessTypes = 21;
	protected $iblockGroupsId = 22;
	protected $iblockMessagesId = 23;
	protected $osSubIos = 31;
	protected $osSubAndroid = 32;
	protected $osProvIos = 33;
	protected $osProvAndroid = 34;
	
	public function __construct() {
		parent::__construct();
		\Bitrix\Main\Loader::includeModule('iblock');
	}
	
	public function subscribe() {
		$this->addCheckFields(array("APP", "OS", "PLAYER", "PROVIDER"));
        $this->checkFields();
		$this->CheckHash();
        $arData = $this->getRequest();
		
		if (strlen($arData["EMAIL"]) > 0) {
			$dbResult = \CUser::GetList($by = '', $order = '', array("=EMAIL" => $arData["EMAIL"]), array("FIELDS" => "ID"));
			if ($arUser = $dbResult->Fetch()) {
				$userId = $arUser["ID"];
			}
		}
		$element = new \CIBlockElement;
		$arFields = array(
			"PROPERTY_VALUES" => array(
				"OS" => $this->GetSubOs($arData["OS"]),
				"SERVICE" => $this->GetProviderId($arData["PROVIDER"]),
				"APP" => $this->GetApplicationId($arData["APP"]),
				"USER" => $userId
			)
		);
		//Проверяем, существует ли подписчик с таким Player ID
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockSubsId, "NAME" => $arData["PLAYER"]),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		if ($arSub = $dbResult->Fetch()) {
			if (!$element->Update($arSub["ID"], $arFields)) {
				$this->setException("ERROR_UPDATE", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
			}
			$this->setResult("OK_OS_USEREXISTS");
		} else {
			$arFields["IBLOCK_ID"] = $this->iblockSubsId;
			$arFields["NAME"] = $arData["PLAYER"];
			if (!$arSub["ID"] = $element->Add($arFields)) {
				$this->setException("ERROR_REGISTRATION", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
			}
			$this->setResult("OK_OS_REGISTER");
		}
		//Проверяем и создаём рассылку в ИБ "Группы подписки"
		if (strlen($arData["NAME"]) > 0) {
			$dbResult = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $this->iblockGroupsId, "PROPERTY_SUBSCRIBER" => $arSub["ID"], "CODE" => $arData["NAME"]),
				false,
				false,
				array("IBLOCK_ID", "ID")
			);
			if (!$arGroup = $dbResult->Fetch()) {
				$arFields = array(
					"IBLOCK_ID" => $this->iblockGroupsId,
					"NAME" => $arData["NAME"],
					"CODE" => $arData["NAME"],
					"PROPERTY_VALUES" => array("SUBSCRIBER" => $arSub["ID"])
				);
				if (!$element->Add($arFields)) {
					$this->setException("ERROR_UPDATE", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
				}
			}
			$this->setResult("OK_OS_SUBSCRIBE");
		}
	}
	
	public function message() {
		$this->addCheckFields(array("NAME", "SENDON", "TEXT", "TYPE"));
        $this->checkFields();
		$this->CheckHash();
        $arData = $this->getRequest();
		
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockMessTypes, "CODE" => $arData["TYPE"]),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		if (!$arMesType = $dbResult->Fetch()) {
			$this->setException("ERROR_OS_MESSTYPE");
		}
		$arFields = array(
			"IBLOCK_ID" => $this->iblockMessagesId,
			"NAME" => $this->encode($arData["NAME"]),
			"ACTIVE" => "Y",
			"DATE_ACTIVE_FROM" => ConvertTimeStamp($arData["SENDON"], "FULL"),
			"PREVIEW_TEXT" => $this->encode($arData["TEXT"]),
			"PROPERTY_VALUES" => array(
				"TYPE" => $arMesType["ID"]
			)
		);
		//Разбираемся, кому отправить сообщение
		if (strlen($arData["PLAYER"]) > 0) {
			$arFields["PROPERTY_VALUES"]["SUBSCRIBER"] = $this->GetSubscriberId($arData["PLAYER"]);
		} elseif (strlen($arData["ID"]) > 0) {
			$arFields["PROPERTY_VALUES"]["GROUP"] = $arData["ID"];
		} else {
			$this->setException("ERROR_OS_NOID");
		}
		//Записываем элемент ИБ
		$element = new \CIBlockELement;
		if (!$elementId = $element->Add($arFields)) {
			$this->setException("ERROR_REGISTRATION", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
		}
		$this->setResult("USER_AUTH_NUMBER_CARD");
		$this->setParams(array("CARD" => $elementId));
	}
	
	public function CheckMessages() {
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockMessagesId, "ACTIVE" => "Y", "<DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")),
			false,
			false,
			array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PROPERTY_TYPE", "PROPERTY_SUBSCRIBER", "PROPERTY_SUBSCRIBER.NAME", "PROPERTY_GROUP")
		);
		$element = new \CIBlockELement;
		while ($arMessage = $dbResult->Fetch()) {
			//В будущем здесь должны использоваться тип сообщения, ОС и т. д.
			if ($arMessage["PROPERTY_SUBSCRIBER_VALUE"] > 0) {
				$response = $this->SendMessage($arMessage["PREVIEW_TEXT"], $arMessage["PROPERTY_SUBSCRIBER_NAME"], $arMessage["NAME"]);
			} else {
				//Достаём из ИБ всех подписчиков группы
				$arGroupPlayers = array();
				$dbGroup = \CIBlockElement::GetList(
					array(),
					array("IBLOCK_ID" => $this->iblockGroupsId, "CODE" => $arMessage["PROPERTY_GROUP_VALUE"]),
					false,
					false,
					array("IBLOCK_ID", "ID", "PROPERTY_SUBSCRIBER", "PROPERTY_SUBSCRIBER.NAME")
				);
				while ($arSub = $dbGroup->Fetch()) {
					$arGroupPlayers[] = $arSub["PROPERTY_SUBSCRIBER_NAME"];
				}
				$response = $this->SendMessage($arMessage["PREVIEW_TEXT"], $arGroupPlayers, $arMessage["NAME"]);
			}
			//Записываем ответ провайдера и деактивируем сообщение
			$element->Update($arMessage["ID"], array("ACTIVE" => "N", "DETAIL_TEXT" => $response));
		}
	}
	
	private function SendMessage($message, $arPlayerIds, $heading = '') {
		if (!is_array($arPlayerIds)) {
			$arPlayerIds = array($arPlayerIds);
		}
		//json_encode работает только с сообщеньками в кодировке UTF
		$content = array('en' => $this->encodeUTF($message));
		$heading = array('en' => $this->encodeUTF($heading));
		$fields = array(
			'app_id' => \Bitrix\Main\Config\Option::get('gds.mobileapi', "OS_ADMINID"),
			'include_player_ids' => $arPlayerIds,
			'data' => array("testvar" => "verycard"),
			'contents' => $content,
			'headings' => $heading
		);
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic '.\Bitrix\Main\Config\Option::get('gds.mobileapi', "OS_RESTKEY")
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//Возвращает JSON с UUID отправленного уведомления
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	
	protected function GetSubscriberId($player) {
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockSubsId, "NAME" => $player),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		if ($arSub = $dbResult->fetch()) {
			return $arSub["ID"];
		}
		$this->setException("ERROR_OS_SUBSCRIBER");
	}
	
	protected function GetApplicationId($code) {
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockAppsId, "CODE" => $code),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		if ($arApp = $dbResult->fetch()) {
			return $arApp["ID"];
		}
		$this->setException("ERROR_OS_APPCODE");
	}
	
	protected function GetProviderId($code) {
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockProvidersId, "CODE" => $code),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		if ($arProvider = $dbResult->fetch()) {
			return $arProvider["ID"];
		}
		$this->setException("ERROR_OS_PROVIDER");
	}
	
	protected function GetSubOs($code) {
		switch($code) {
			case "android":
				return $this->osSubAndroid;
			default: //ios
				return $this->osSubIos;
		}
	}
	
	protected function GetProviderOs($code) {
		switch($code) {
			case "android":
				return $this->osProvAndroid;
			default: //ios
				return $this->osProvIos;
		}
	}
}
?>