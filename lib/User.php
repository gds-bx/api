<?
namespace Bestgarden\Gds\MobileApi;

Class User extends Api {
	public function __construct() {
		parent::__construct();
		\Bitrix\Main\Loader::includeModule('iblock');
	}
	
	protected function auth() {
		$this->addCheckFields(array("LOGIN"));
		$this->checkFields();
		$arData = $this->getRequest();
		$userId = $this->getIdByEmail($arData["LOGIN"]);
		$this->CheckHash($this->GetPassword("user", $userId));
		//Проверяем, что у пользователя есть номер карты
		$dbResult = \CUser::GetList(
			$by = '', $order = '',
			array("ID" => $userId, "!UF_NUMBER_CART" => false),
			array("SELECT" => array("UF_NUMBER_CART"), "FIELDS" => array("ACTIVE"))
		);
		if (!$user = $dbResult->Fetch()) {
			$this->setException('ERROR_NOT_CARD');
		}
		if ($user["ACTIVE"] != "Y") {
			$obUser = new \CUser;
			$obUser->Update($userId, array("ACTIVE" => "Y"));
		}
		$this->setResult("USER_AUTH_NUMBER_CARD");
		$this->setParams(array("CARD" => $user["UF_NUMBER_CART"]));
    }

    protected function register() {
        $this->addCheckFields(array('EMAIL', 'NAME'));
        $this->checkFields();
		$this->checkHash();
        $arData = $this->getRequest();
        if (defined("CAPTCHA_COMPATIBILITY")) {
            $_SESSION["CAPTCHA_CODE"]["MOBILEAPI"] = "MOBILEAPI";
        } else {
            include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
            \CCaptcha::Add(Array("CODE" => 'MOBILEAPI', "ID" => 'MOBILEAPI'));
        }
		
        $obUser = new \CUser();
        $arResult = $obUser->Register(
            $this->encode($arData['EMAIL']),
			$this->encode($arData['NAME']),
			$this->encode($arData['SURNAME']),
            "mobileapi",
            "mobileapi",
            $this->encode($arData['EMAIL']),
            false,
            "MOBILEAPI",
            "MOBILEAPI"
        );
        if ($arResult["TYPE"] == "OK") {
			$card = new CardIterator();
			$obUser->Update($arResult["ID"], Array("UF_NUMBER_CART" => $card->number));
			$obUser->Logout();
			$this->setResult("USER_AUTH_NUMBER_CARD");
			$this->setParams(array("CARD" => $card->number));
        } else {
            $error = $this->encodeUTF($arResult["MESSAGE"]);
            $error = preg_replace('/\<br(\s*)?\/?\>/i', "", $error);
            $this->setException('ERROR_REGISTRATION', array('MESSAGE_ERROR' => $error));
        }
    }

    protected function forgot() {
        $this->addCheckFields(array('EMAIL'));
        $this->checkFields();
		$this->checkHash();
        $arData = $this->getRequest();

        $arResult = \CUser::SendPassword("", $arData["EMAIL"]);
        if($arResult["TYPE"] == "OK") {
            $this->setResult('OK_FORGOT');
        } else {
            $this->setException('INCORRECT_EMAIL');
        }
    }

	public function get() {
		$this->addCheckFields(array("LOGIN", "SELECT"));
		$this->checkFields();
		$this->checkHash();
		$arData = $this->getRequest();
		
		$rsUser = \CUser::GetByLogin($arData["LOGIN"]);
		if (!$arUser = $rsUser->Fetch()) {
			$this->setException('ERROR_GET_USER');
		}
		$arSelect = explode(",", $arData["SELECT"]);
		$arList = array();
		foreach ($arSelect as $value) {
			$arList[$value] = $arUser[$value];
		}
		$this->setResult($arList);
    }
	
	public function remove() {
		$arData = $this->getRequest();
		$cleaner = new Cleaner($arData["NUMBER_CARD"]);
		if (!$cleaner->cleanData())
			$this->setException('CLEAN_ERROR');
		$this->setResult('CLEAN_OK');
	}
	
	public function getIdByCard($card) {
		$dbResult = \CUser::GetList($by = '', $order = '', array("UF_NUMBER_CART" => $card), array("FIELDS" => "ID"));
		if ($arUser = $dbResult->Fetch()) {
			return $arUser["ID"];
		}
		$this->setException("ERROR_USER_CARD");
	}
	
	public function getIdByEmail($email) {
		$dbResult = \CUser::GetList($by = '', $order = '', array("=EMAIL" => $email), array("FIELDS" => "ID"));
		if ($arUser = $dbResult->Fetch()) {
			return $arUser["ID"];
		}
		$this->setException("ERROR_GET_USER");
	}
	
	public function checkEmail($card, $email) {
		$dbResult = \CUser::GetList($by = '', $order = '', array("UF_NUMBER_CART" => $card), array("FIELDS" => "EMAIL"));
		if (!$arUser = $dbResult->Fetch()) {
			$this->setException("ERROR_USER_CARD");
		}
		if ($arUser["EMAIL"] !== $email) {
			$this->setException("ERROR_USER_EMAIL");
		}
	}
}
