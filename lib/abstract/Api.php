<?
namespace Bestgarden\Gds\MobileApi;
use Bitrix\Main\Application;

abstract class Api {
	const IBLOCK_SHOPS_ID = 9;
	const IBLOCK_HASH_ID = 16;
    private $_POST = array();
	private $_GET = array();
    protected $arRequest = array();
	private $arCheckFields = array("TIME", "HASH");
	protected $arDictionary = array();
	protected $arHashFields = array("ADDRESS", "AMOUNT", "APP", "CARD", "CITY", "COUNTRY", "DETAIL_TEXT", "DOC_DATE", "DOC_NUMBER",
		"DOC_TYPE", "EMAIL", "FULLNAME", "ID", "LATITUDE", "LOGIN", "LONGITUDE", "MARK", "NAME", "OS", "PASS", "PHONE", "PLAYER",
		"PREVIEW_TEXT", "PROVIDER", "RATING", "SECTION", "SELECT", "SENDON", "SHOPCODE", "SHOPURL", "SURNAME", "TEXT", "TIME", "TYPE");

    function __construct(){
        $request = Application::getInstance()->getContext()->getRequest();
        $this->_POST = array_map('trim', $request->getPostList()->toArray());
		$this->_GET = array_map('trim', $request->getQueryList()->toArray());
		$this->arRequest = array_merge($this->_GET, $this->_POST);
		$this->arDictionary = include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/gds.mobileapi/lib/Dictionary.php");
    }

    function __call($func, $params){
		$this->setException('NOT_METHOD', array('FUNC' => $func));
	}

    public function callMethod($method = ''){
		$method = htmlspecialchars($method);
		if (strlen($method) < 1) {
			$this->setException('EMPTY_METHOD');
		}
		//$this->security();
		$this->$method();
		$this->send();
	}
	
	protected function GetPassword($type, $id) {
		if ($type == "user") {
			$dbResult = \CUser::GetList($by = '', $order = '', array("ID" => $id), array("SELECT" => array("UF_CODE")));
			if (!$arUser = $dbResult->Fetch()) {
				$this->setException("ERROR_GET_USER");
			}
			return $this->unEncrypt($arUser["UF_CODE"]);
		}
		//else type == "shop"
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => self::IBLOCK_SHOPS_ID, "ID" => $id),
			false,
			false,
			array("IBLOCK_ID", "ID", "PROPERTY_MAG_PASS")
		);
		if (!$arShop = $dbResult->Fetch()) {
			$this->setException("ERROR_SHOP_CODE");
		}
		return $this->unEncrypt($arShop["PROPERTY_MAG_PASS_VALUE"]);
	}
	
	protected function CheckHash($password = '') {
		$arData = $this->getRequest();
		//Проверка отклонения TIME
		$period = \Bitrix\Main\Config\Option::get('gds.mobileapi', "TIME_PERIOD");
		if ($arData["TIME"] < AddToTimeStamp(array("HH" => -$period)) or $arData["TIME"] > AddToTimeStamp(array("HH" => $period))) {
			$this->setException("INVALID_TIME");
		}
		//Проверка хэша
		$s = '';
		foreach($arData as $key => $value) {
			if (in_array($key, $this->arHashFields)) {
				$s .= $value;
			}
		}
		$hash = ToLower(md5($s.$password.$this->getSecretCode()));
		if ($hash != $arData["HASH"]) {
			$this->setException("INVALID_HASH");
		}
		$this->CheckHashIB($hash);
	}
	
	private function CheckHashIB($hash) {
		global $APPLICATION;
		$arData = array_merge($this->getRequest(), $_FILES);
		$element = new \CIBlockElement;
		$arFields = array(
			"IBLOCK_ID" => self::IBLOCK_HASH_ID,
			"NAME" => $hash,
			"CODE" => $hash,
			"ACTIVE" => "Y",
			"DETAIL_TEXT" => serialize(array_merge(array("METHOD" => $APPLICATION->GetCurDir()), $arData))
		);
		if(!$hashId = $element->Add($arFields)) {
			$this->setException('ERROR_HASH_CHECK');
		}
	}
	
	public static function CheckOldHash() {
		\Bitrix\Main\Loader::includeModule('iblock');
		$limit = ConvertTimeStamp(AddToTimeStamp(array("DD" => -\Bitrix\Main\Config\Option::get('gds.mobileapi', "HASH_PERIOD"))), "FULL");
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => self::IBLOCK_HASH_ID, "<DATE_CREATE" => $limit),
			false,
			false,
			array("IBLOCK_ID", "ID")
		);
		while ($hashElem = $dbResult->Fetch()) {
			\CIBlockElement::Delete($hashElem["ID"]);
		}
	}

    function checkFields(){
        $arData = $this->getRequest();
        $arCheckFields = $this->getCheckFields();
		
        $arErrorCode = array();
        foreach($arCheckFields as $code){
            if(strlen($arData[$code]) < 1){
                $arErrorCode[] = $code;
            }
        }
        if(in_array('EMAIL', $arCheckFields) && strlen($arData['EMAIL']) > 0){
            if(!check_email($arData['EMAIL'])){
                $this->setException('INVALID_FORMAT_EMAIL');
            }
        }
        if(count($arErrorCode) > 0){
            $this->setException('NOT_CODE', array('CODE' => implode($arErrorCode, ', ')));
        }
    }

	public function getSecretCode(){
		return \Bitrix\Main\Config\Option::get('gds.mobileapi', "SECRET_WORD");
	}

    function getCheckFields(){
        return array_map('trim', $this->arCheckFields);
    }

    function addCheckFields(array $arFields){
		$this->arCheckFields = array_merge($this->arCheckFields, $arFields);
	}

    function getRequest(){
        return $this->arRequest;
    }

    function getResult(){
        return $this->result;
    }

    function getParams(){
        return $this->params;
    }
	
    function setResult($result){
        $this->result = $result;
    }

    function setParams($params){
        $this->params = $params;
    }

    protected function setException($message, $params = array()){
        throw new ExceptionApi($message, 0, null, $params);
    }

    function send(){
        Response::succes($this->getResult(), $this->getParams());
    }

    function encrypt($password) {
        $arPassword = str_split($password);
        foreach($arPassword as $v){
			$key = array_search($v, $this->arDictionary);
            if($key !== 'false') {
                $tmpPassword .= $key;
            }
        }
        return $tmpPassword;
    }
	
	function unEncrypt($password) {
        $tmpPassword = '';
        $arPassword = str_split($password);
        foreach($arPassword as $v){
            if(isset($this->arDictionary[$v])) {
                $tmpPassword .= $this->arDictionary[$v];
            }
        }
        return $tmpPassword;
    }
	
	function encode($str) {
		return iconv("UTF-8", "Windows-1251", (string) $str);
	}
	
	function encodeUTF($str) {
		return iconv("Windows-1251", "UTF-8", (string) $str);
	}
}
