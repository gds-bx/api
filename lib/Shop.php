<?
namespace Bestgarden\Gds\MobileApi;

Class Shop extends Api {
	protected $iblockShopsId = 9;
	protected $iblockDescriptionsId = 12;
	protected $newpassMessId = 8;
	
	public function __construct() {
		parent::__construct();
		\Bitrix\Main\Loader::includeModule('iblock');
	}
   
    public function auth() {
		$this->addCheckFields(array("LOGIN"));
        $this->checkFields();
        $arData = $this->getRequest();
		$shop = $this->getByCode($arData["LOGIN"]);
		$this->CheckHash($this->GetPassword("shop", $shop["ID"]));
		
		$this->setResult("USER_AUTH_NUMBER_CARD");
		$this->setParams(array("CARD" => "true"));
    }
	
	public function register() {
		$this->addCheckFields(array("EMAIL", "LOGIN", "NAME", "PASS"));
        $this->checkFields();
        $arData = $this->getRequest();
		$user = new User;
		$userId = $user->getIdByEmail($arData["EMAIL"]);
		$this->CheckHash($this->GetPassword("user", $userId));
		//Создаём элемент в ИБ "Компании"
		$element = new \CIBlockElement;
		$arFields = array(
			"IBLOCK_ID" => $this->iblockShopsId,
			"NAME" => $this->encode($arData["NAME"]),
			"CODE" => $this->encode($arData["LOGIN"]),
			"PROPERTY_VALUES" => array(
				"MAG_ID" => $this->encode($arData["LOGIN"]),
				"MAG_PASS" => $this->encrypt($arData["PASS"]),
				"SHOP_ADMIN" => $userId,
				"PERIOD" => 365
			)
		);
		if (!$elementId = $element->Add($arFields)) {
			$this->setException("ERROR_REGISTRATION", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
		}
		$this->setResult("OK_SHOP_REGISTER");
	}
	
	public function fastregister() {
		$this->addCheckFields(array("EMAIL", "NAME"));
        $this->checkFields();
        $arData = $this->getRequest();
		$user = new User;
		$userId = $user->getIdByEmail($arData["EMAIL"]);
		$this->CheckHash($this->GetPassword("user", $userId));
		//Генерируем логин и пароль
		$password = $this->encrypt(randString(10));
		$login = \CUtil::translit($this->encode($arData["NAME"]), 'ru');
		$exists = true; $i = '';
		while ($exists) {
			$exists = false;
			$dbResult = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $this->iblockShopsId, "PROPERTY_MAG_ID" => $login.$i),
				false,
				false,
				array("IBLOCK_ID", "ID")
			);
			if ($IBLogin = $dbResult->Fetch()) {
				$exists = true;
				$i++;
			}
		}
		//Создаём элемент в ИБ "Компании"
		$element = new \CIBlockElement;
		$arFields = array(
			"IBLOCK_ID" => $this->iblockShopsId,
			"NAME" => $this->encode($arData["NAME"]),
			"CODE" => $login.$i,
			"PROPERTY_VALUES" => array(
				"MAG_ID" => $login.$i,
				"MAG_PASS" => $password,
				"SHOP_ADMIN" => $userId,
				"PERIOD" => 365
			)
		);
		if (!$elementId = $element->Add($arFields)) {
			$this->setException("ERROR_REGISTRATION", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
		}
		
		$arList = array(
			"login" => $login.$i,
			"password" => $password
		);
		$this->setResult($arList);
	}
	
	public function newpass() {
		$this->addCheckFields(array("EMAIL", "SHOPCODE"));
        $this->checkFields();
        $arData = $this->getRequest();
		$user = new User;
		$userId = $user->getIdByEmail($arData["EMAIL"]);
		$this->CheckHash($this->GetPassword("user", $userId));
		//Проверяем на администратора магазина
		$shop = $this->getByCode($arData["SHOPCODE"]);
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockShopsId, "ID" => $shop["ID"], "PROPERTY_SHOP_ADMIN" => $userId),
			false,
			false,
			array("IBLOCK_ID", "ID", "PROPERTY_SHOP_ADMIN")
		);
		if (!$shopChecked = $dbResult->Fetch()) {
			$this->setException("ERROR_SHOP_AUTH");
		}
		//Новый пароль
		$password = randString(10);
		if (!\CEvent::Send("SHOP_PASS", 's1', array("EMAIL" => $arData["EMAIL"], "NAME" => $shop["NAME"], "PASSWORD" => $password), "Y", $this->newpassMessId)){
			$this->setException("ERROR_SENDMAIL");
		}
		\CIBlockElement::SetPropertyValuesEx(
			$shop["ID"],
			$this->iblockShopsId,
			array("MAG_PASS" => $this->encrypt($password))
		);
		$this->setResult("OK_FORGOT");
	}
	
	public function describe() {
		$this->addCheckFields(array("NAME", "SECTION", "SHOPCODE"));
        $this->checkFields();
		$arData = array_merge($this->getRequest(), $_FILES);
		//Проверяем наличие всех полей в запросе
		$arProps = array("ADDRESS", "CITY", "COUNTRY", "DETAIL_TEXT", "EMAIL", "FULLNAME", "LATITUDE", "LOGOIMAGELINK",
			"LONGITUDE", "PHONE", "PREVIEW_TEXT", "SHOPURL");
		$arPropErrors = array();
		for ($i = 0, $c = count($arProps); $i < $c; $i++) {
			if (!array_key_exists($arProps[$i], $arData)) {
				$arPropErrors[] = $arProps[$i];
			}
		}
        if (count($arPropErrors) > 0) {
			$this->setException("ERROR_SHOP_FIELDS", array("FIELDS" => implode($arPropErrors, ', ')));
		}
		$shopId = $this->getByCode($arData["SHOPCODE"]);
		$shopId = $shopId["ID"];
		$this->CheckHash($this->GetPassword("shop", $shopId));
		//Ищем ID раздела по имени
		$dbResult = \CIBlockSection::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockDescriptionsId, "NAME" => $this->encode($arData["SECTION"])),
			false,
			array("ID")
		);
		if (!$sectionId = $dbResult->Fetch()) {
			$this->setException("ERROR_SHOP_SECTION");
		}
		$sectionId = $sectionId["ID"];
		//Ищем точку продаж с переданным ID
		if ($arData["ID"]) {
			$dbResult = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $this->iblockDescriptionsId, "ID" => $arData["ID"]),
				false,
				false,
				array("IBLOCK_ID", "ID")
			);
			if($descriptionId = $dbResult->Fetch()) {
				$descriptionId = $descriptionId["ID"];
			}
		}
		$element = new \CIBlockElement;
		$arFields = array(
			"NAME" => $this->encode($arData["NAME"]),
			"IBLOCK_SECTION_ID" => $sectionId,
			"PREVIEW_TEXT" => $this->encode($arData["PREVIEW_TEXT"]),
			"DETAIL_TEXT" => $this->encode($arData["DETAIL_TEXT"]),
			"PROPERTY_VALUES" => array(
				"SHOPID" => $shopId,
				"FULLNAME" => $this->encode($arData["FULLNAME"]),
				"COUNTRY" => $this->encode($arData["COUNTRY"]),
				"CITY" => $this->encode($arData["CITY"]),
				"ADDRESS" => $this->encode($arData["ADDRESS"]),
				"SHOPURL" => $this->encode($arData["SHOPURL"]),
				"PHONE" => $this->encode($arData["PHONE"]),
				"LONGITUDE" => $this->encode($arData["LONGITUDE"]),
				"LATITUDE" => $this->encode($arData["LATITUDE"]),
				"LOGOIMAGELINK" => $arData["LOGOIMAGELINK"],
				"EMAIL" => $this->encode($arData["EMAIL"])
			)
		);
		if ($descriptionId) {
			//Обновляем старый элемент
			\CIBlockElement::SetPropertyValuesEx(
				$descriptionId,
				$this->iblockDescriptionsId,
				array("LOGOIMAGELINK" => array("VALUE" => array("del" => "Y")))
			);
			if(!$description = $element->Update($descriptionId, $arFields)) {
				$this->setException("ERROR_UPDATE", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
			}
			$this->setResult("OK_SHOP_DESCRIBE");
		} else {
			//Создаём новый элемент
			$arFields["IBLOCK_ID"] = $this->iblockDescriptionsId;
			if (!$description = $element->Add($arFields)) {
				$this->setException("ERROR_UPDATE", array("MESSAGE_ERROR" => $this->encodeUTF($element->LAST_ERROR)));
			}
			$this->setResult("OK_SHOP_NEWDESCRIBE");
		}
	}
	
	public function getlist() {
		$this->addCheckFields(array("EMAIL"));
        $this->checkFields();
        $arData = $this->getRequest();
		$user = new User;
		$userId = $user->getIdByEmail($arData["EMAIL"]);
		$this->CheckHash($this->GetPassword("user", $userId));
		
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockShopsId, "PROPERTY_SHOP_ADMIN" => $userId),
			false,
			false,
			array("IBLOCK_ID", "ID", "NAME", "PROPERTY_MAG_ID", "PROPERTY_MAG_PASS")
		);
		$arList = array();
		while ($shop = $dbResult->Fetch()) {
			$arList[] = array(
				"CODE" => $shop["PROPERTY_MAG_ID_VALUE"],
				"NAME" => $shop["NAME"],
				"PASSWORD" => $shop["PROPERTY_MAG_PASS_VALUE"]
			);
		}
		$this->setResult($arList);
	}

	public function getByCode($code) {
		$rsShop = \CIBlockElement::GetList(
			array(),
			array('IBLOCK_ID' => $this->iblockShopsId, 'PROPERTY_MAG_ID' => $code),
			false,
			false,
			array('ID', 'NAME')
		);
		if ($arShop = $rsShop->fetch()) {
			return array('ID' => $arShop['ID'], 'NAME' => $arShop['NAME']);
		}
		$this->setException("ERROR_SHOP_CODE");
	}
}