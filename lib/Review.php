<?
namespace Bestgarden\Gds\MobileApi;

Class Review extends Api {
	protected $iblockReviewsId = 14;
	
	public function __construct() {
	   parent::__construct();
	   \Bitrix\Main\Loader::includeModule('iblock');
	}
	
	public function add() {
		$this->addCheckFields(array("DOC_DATE", "EMAIL", "RATING", "SHOPCODE", "TEXT"));
		$this->checkFields();
		$arData = $this->getRequest();
		$user = new User;
		$userId = $user->getIdByEmail($arData['EMAIL']);
		$this->CheckHash($this->GetPassword("user", $userId));
		
		$shop = new Shop;
		$shop = $shop->getByCode($arData['SHOPCODE']);
		$element = new \CIBlockElement;
		$arFields = array(
			'NAME' => date('d.m.Y'), 
			'IBLOCK_ID' => $this->iblockReviewsId,
			'ACTIVE' => "N",
			'ACTIVE_FROM' => $arData['DOC_DATE'],
			'PREVIEW_TEXT' => $this->encode($arData['TEXT']),
			'PROPERTY_VALUES' => array(
				'CLIENT' => $userId,
				'MARK' => $this->encode($arData['RATING']),
				'SHOP' => $shop["ID"]
			)
		);
		if (!$result = $element->Add($arFields)) {
			$this->setException('ERROR_REVIEW_ADD');
		}
		$this->setResult("OK_REVIEW_ADD");
	}
}