<?
namespace Bestgarden\Gds\MobileApi;

class Transaction extends Api {
	protected $testCardNumber = '0000008'; //��� ������������ ����������� �� 1 ��������� �����
	protected $iblockTransactionsId = 8;
	protected $iblockShopsId = 9;
	protected $typeAddId = 6;
	protected $typeCorrectId = 7;
	protected $typeWriteoffId = 9;
	protected $docTypeCheck = 3;
	protected $docTypeOrder = 4;
	protected $docTypeSocial = 5;
	protected $periodLimitUnix = array(); //������ ����� ����������� ������ ��� ������� ��������
	
	public function __construct() {
		parent::__construct();
		\Bitrix\Main\Loader::includeModule('iblock');
	}
	public function add() {
		$this->addTransaction($this->typeAddId);
	}
	public function writeoff() {
		$this->addTransaction($this->typeWriteoffId);
	}
	protected function addTransaction($typeId) {
		$this->addCheckFields(array("SHOPCODE", "CARD", "EMAIL", "AMOUNT", "DOC_TYPE", "DOC_NUMBER", "DOC_DATE"));
		$this->checkFields();
		$arData = $this->getRequest();
		$shop = new Shop;
		$shopId = $shop->getByCode($arData["SHOPCODE"])['ID'];
		$this->CheckHash($this->GetPassword("shop", $shopId));
		
		$user = new User;
		$user = $user->checkEmail($arData["CARD"], $arData["EMAIL"]);
		if ($typeId === $this->typeWriteoffId and $arData["AMOUNT"] > 0) {
			$arData["AMOUNT"] = 0 - $arData["AMOUNT"];
		}
		$element = new \CIBlockElement;
		$date = ConvertTimeStamp(time()+\CTimeZone::GetOffset(), "FULL");
		$arFields = array(
			"IBLOCK_ID" => $this->iblockTransactionsId,
			"NAME" => "Transaction",
			"ACTIVE" => "Y",
			"DATE_ACTIVE_FROM" => $date,
			"PROPERTY_VALUES" => array(
				"MAGAZINE" => $shopId,
				"BUYER_CARD" => $this->encode($arData["CARD"]),
				"BUYER_MAIL" => $this->encode($arData["EMAIL"]),
				"TRANSACTION_TYPE" => $typeId,
				"TRNSACTION_SUMM" => $this->encode($arData["AMOUNT"]),
				"TRNSACTION_DATE" => $date,
				"OSNOVANIE" => $this->GetDocType($arData["DOC_TYPE"]),
				"DOC_NUMBER" => $this->encode((string)$arData["DOC_NUMBER"]),
				"DOC_DATE" => $this->encode((string)$arData["DOC_DATE"])
			)
		);
		if (!$elementId = $element->Add($arFields)) {
			$this->setException("ERROR_TRANSACTION_ADD");
		}
		if ($typeId === $this->typeWriteoffId) {
			$this->setResult("OK_TRANSACTION_WRITEOFF");
		} else {
			$this->setResult("OK_TRANSACTION_ADD");
		}
	}
	
	public function balance() {
		$dbResult = $this->getTransactions(true);
		$balance = 0;
		while ($transaction = $dbResult->Fetch()) {
			$balance += intval($transaction['PROPERTY_TRNSACTION_SUMM_VALUE']);
		}
		$this->setParams(array("SUMM" => $balance));
		$this->setResult("OK_TRANSACTION_BALANCE");
	}
	
	public function getlist() {
		$arData = $this->getRequest();
		if (isset($arData["SHOPCODE"])) {
			$dbResult = $this->getTransactions();
		} else {
			$dbResult = $this->getTransactions(false, true);
		}
		$arList = array();
		while ($transaction = $dbResult->Fetch()) {
			$arList[] = array(
				"ID" => $transaction["ID"],
				"ACTIVE" => $transaction["ACTIVE"],
				"TRANSACTION_DATE" => $transaction["PROPERTY_TRNSACTION_DATE_VALUE"],
				"SHOP" => $transaction["PROPERTY_MAGAZINE_NAME"].' ['.$transaction["PROPERTY_MAGAZINE_PROPERTY_MAG_ID_VALUE"].']',
				"TRANSACTION_TYPE" => $transaction["PROPERTY_TRANSACTION_TYPE_VALUE"],
				"AMOUNT" => $transaction["PROPERTY_TRNSACTION_SUMM_VALUE"],
				"CARD" => $transaction["PROPERTY_BUYER_CARD_VALUE"],
				"EMAIL" => $transaction["PROPERTY_BUYER_MAIL_VALUE"],
				"DOC_TYPE" => $transaction["PROPERTY_OSNOVANIE_VALUE"],
				"DOC_NUMBER" => $transaction["PROPERTY_DOC_NUMBER_VALUE"],
				"DOC_DATE" => $transaction["PROPERTY_DOC_DATE_VALUE"]
			);
		}
		$this->setResult($arList);
	}
	
	protected function getTransactions($flagActive = false, $bCardOnly = false) {
		$this->addCheckFields(array("CARD", "EMAIL"));
		if (!$bCardOnly) {
			$this->addCheckFields(array("SHOPCODE"));
		}
		$this->checkFields();
		$arData = $this->getRequest();
		$user = new User;
		$user->checkEmail($arData["CARD"], $arData["EMAIL"]);
		
		if ($bCardOnly) {
			$password = $this->GetPassword("user", $user->getIdByEmail($arData["EMAIL"]));
		} else {
			$shop = new Shop;
			$shop = $shop->getByCode($arData["SHOPCODE"]);
			$password = $this->GetPassword("shop", $shop["ID"]);
		}
		$this->CheckHash($password);
		//��������� ������ � ����������� �� ���������� �������
		$arFilter = array(
			"IBLOCK_ID" => $this->iblockTransactionsId,
			"=PROPERTY_BUYER_CARD" => $arData["CARD"],
			"=PROPERTY_BUYER_MAIL" => $arData["EMAIL"]
		);
		if ($flagActive) {
			$arFilter["ACTIVE"] = "Y";
		}
		if (!$bCardOnly) {
			$arFilter["PROPERTY_MAGAZINE"] = $shop["ID"];
		}
		$dbResult = \CIBlockElement::GetList(
			array('PROPERTY_TRNSACTION_DATE' => 'DESC'),
			$arFilter,
			false,
			false,
			array(
				'ID',
				'ACTIVE',
				'PROPERTY_MAGAZINE',
				'PROPERTY_MAGAZINE.NAME',
				'PROPERTY_MAGAZINE.PROPERTY_MAG_ID',
				'PROPERTY_BUYER_CARD',
				'PROPERTY_BUYER_MAIL',
				'PROPERTY_TRANSACTION_TYPE',
				'PROPERTY_TRNSACTION_SUMM',
				'PROPERTY_TRNSACTION_DATE',
				'PROPERTY_OSNOVANIE',
				'PROPERTY_DOC_NUMBER',
				'PROPERTY_DOC_DATE'
			)
		);
		if($dbResult->SelectedRowsCount() < 1){
			$this->setException("ERROR_TRANSACTION_GET");
		}
		return $dbResult;
	}
	
	public function CheckOld() {
		//�������� �� �������, ��� ����� � ���������� ���� "��������" ��������� ��������������
		$arCorrect = array();
		$arDeactivationId = array();
		$this->GetPeriodLimits();
		if ($this->testCardNumber != '') { //��� ������������ ����������� �� 1 ��������� �����
			$dbResult = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $this->iblockTransactionsId, "ACTIVE" => "Y", "PROPERTY_BUYER_CARD" => $this->testCardNumber),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_MAGAZINE", "PROPERTY_BUYER_CARD", "PROPERTY_BUYER_MAIL",
					"PROPERTY_TRNSACTION_DATE", "PROPERTY_TRNSACTION_SUMM", "PROPERTY_TRANSACTION_TYPE")
			);
		} else {
			$dbResult = \CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID" => $this->iblockTransactionsId, "ACTIVE" => "Y"),
				false,
				false,
				array("ID", "IBLOCK_ID", "PROPERTY_MAGAZINE", "PROPERTY_BUYER_CARD", "PROPERTY_BUYER_MAIL",
					"PROPERTY_TRNSACTION_DATE", "PROPERTY_TRNSACTION_SUMM", "PROPERTY_TRANSACTION_TYPE")
			);
		}
		while ($transaction = $dbResult->Fetch()) {
			$shopId = $transaction["PROPERTY_MAGAZINE_VALUE"];
			$card = $transaction["PROPERTY_BUYER_CARD_VALUE"];
			$dateUnix = MakeTimeStamp($transaction["PROPERTY_TRNSACTION_DATE_VALUE"]);
			if ($dateUnix <= $this->periodLimitUnix[$shopId] or $transaction["PROPERTY_TRNSACTION_SUMM_VALUE"] <= 0) { //Old or New-
				$arCorrect[$shopId][$card]["SUM"] += $transaction["PROPERTY_TRNSACTION_SUMM_VALUE"];
				if ($transaction["PROPERTY_TRANSACTION_TYPE_ENUM_ID"] == $this->typeCorrectId) {
					$arCorrect[$shopId][$card]["ID"] = $transaction["ID"];
				} else {
					$arDeactivationId[] = $transaction["ID"];
				}
			}
			$arCorrect[$shopId][$card]["EMAIL"] = $transaction["PROPERTY_BUYER_MAIL_VALUE"];
		}
		$element = new \CIBlockElement;
		if(!empty($arDeactivationId)) {
			$this->UpdateList(array("ACTIVE" => "N"), array("IBLOCK_ID" => $this->iblockTransactionsId, "ID" => $arDeactivationId));
		}
		//���� � ������� ��� �� ���� ���������� "�������������", ������ �����, ���� ���� - ���������
		foreach ($arCorrect as $shop => $arBuyers) {
			foreach ($arBuyers as $card => $record) {
				if ($record["SUM"] >= 0) {
					$arCorrect[$shop][$card]["SUM"] = 0;
				}
				if ($record["ID"] > 0) {
					\CIBlockElement::SetPropertyValuesEx(
						$record["ID"],
						$this->iblockTransactionsId,
						array("TRNSACTION_SUMM" => $arCorrect[$shop][$card]["SUM"], "TRNSACTION_DATE" => ConvertTimeStamp(time()+\CTimeZone::GetOffset(), "FULL"))
					);
				} else {
					$element->Add(array(
						"IBLOCK_ID" => $this->iblockTransactionsId,
						"ACTIVE" => "Y",
						"NAME" => "Transaction",
						"PROPERTY_VALUES" => array(
							"MAGAZINE" => $shop,
							"BUYER_CARD" => $card,
							"BUYER_MAIL" => $record["EMAIL"],
							"TRANSACTION_TYPE" => $this->typeCorrectId,
							"TRNSACTION_SUMM" => intval($arCorrect[$shop][$card]["SUM"]),
							"TRNSACTION_DATE" => ConvertTimeStamp(time()+\CTimeZone::GetOffset(), "FULL")
						)
					));
				}
			}
		}
	}
	
	protected function GetDocType($xmlId = '') {
		switch($xmlId) {
			case "check":
				return $this->docTypeCheck;
			case "social":
				return $this->docTypeSocial;
			default:
				return $this->docTypeOrder;
		}
	}
	protected function GetPeriodLimits() {
		$dbResult = \CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $this->iblockShopsId),
			false,
			false,
			array("ID", "IBLOCK_ID", "PROPERTY_PERIOD")
		);
		while ($shop = $dbResult->Fetch()) {
			$this->periodLimitUnix[$shop["ID"]] = AddToTimeStamp(array("DD" => -$shop["PROPERTY_PERIOD_VALUE"]));
		}
	}
	protected function UpdateList($arFields, $arFilter) {
        global $DB;
        $strUpdate = $DB->PrepareUpdate("b_iblock_element", $arFields, "iblock", false, "BE");
        if ($strUpdate == "") {
			return false;
		}
        $element = new \CIBlockElement;
        $element->strField = "ID";
        $element->GetList(array(), $arFilter, false, false, array("ID"));
        $strSql = "
            UPDATE ".$element->sFrom." SET ".$strUpdate."
            WHERE 1=1 ".$element->sWhere."
        ";
        return $DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
    }
}