<?
$moduleId = "gds.mobileapi";

$arClasses = array(
	'\Bestgarden\Gds\MobileApi\ExceptionApi' => "lib/ExceptionApi.php",
	'\Bestgarden\Gds\MobileApi\Api' => "lib/abstract/Api.php",
	'\Bestgarden\Gds\MobileApi\Response' => "lib/Response.php",
	'\Bestgarden\Gds\MobileApi\User' => "lib/User.php",
	'\Bestgarden\Gds\MobileApi\CardIterator' => "lib/Card.php",
	'\Bestgarden\Gds\MobileApi\Shop' => "lib/Shop.php",
	'\Bestgarden\Gds\MobileApi\Review' => "lib/Review.php",
	'\Bestgarden\Gds\MobileApi\Mark' => "lib/Mark.php",
	'\Bestgarden\Gds\MobileApi\Transaction' => "lib/Transaction.php",
	'\Bestgarden\Gds\MobileApi\OneSignal' => "lib/OneSignal.php",
	'\Bestgarden\Gds\MobileApi\Cleaner' => "lib/test/Cleaner.php"
);

CModule::AddAutoloadClasses($moduleId, $arClasses);
