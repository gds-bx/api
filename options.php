<style type="text/css">
.method-link {cursor: pointer; color: navy; border-bottom: 1px dotted blue;}
td.settings {vertical-align: middle; width: 50%;}
td.os-settings {vertical-align: middle;}
</style>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
$(document).ready(function(){
	$(".method-text").hide();
	$(".method-link").click(function(){
		$(this).siblings("div").slideToggle(500);
	});
});
</script>
<?
$module_id = "gds.mobileapi";
$GDS_MOBILEAPI_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($GDS_MOBILEAPI_RIGHT>="R") :

    global $MESS;
    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/options.php");

    if ($REQUEST_METHOD=="GET" && strlen($RestoreDefaults)>0 && $GDS_MOBILEAPI_RIGHT=="W" && check_bitrix_sessid()) {
        COption::RemoveOption($module_id);
        $z = CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
        while($zr = $z->Fetch()) {
            $APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
		}
    }
	$arOptions = array(
		array("SECRET_WORD", GetMessage("GDS_MOBILEAPI_SECRET_WORD"), "", Array("text")),
		array("HASH_PERIOD", GetMessage("GDS_MOBILEAPI_HASH_PERIOD"), "", Array("text")),
		array("TIME_PERIOD", GetMessage("GDS_MOBILEAPI_TIME_PERIOD"), "", Array("text"))
	);
    $strWarning = "";
	if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && $GDS_MOBILEAPI_RIGHT=="W" && check_bitrix_sessid() && strlen($use_sonnet_button) <= 0) {
		foreach($arOptions as $option){
			$name = $option[0];
			$val = $$name;
			if ($option[3][0] == "checkbox" && $val != "Y"){
				$val = "N";
			}
			COption::SetOptionString($module_id, $name, $val, $option[1]);
		}
	}
    if (strlen($strWarning) > 0)
        CAdminMessage::ShowMessage($strWarning);
    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_SETTINGS"), "ICON" => "gds_transactions_settings", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_SETTINGS")),
        array("DIV" => "edit2", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_DATA"), "ICON" => "gds_transactions_social", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_DATA")),
        array("DIV" => "edit3", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_USER"), "ICON" => "gds_mobileapi_manual", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_USER")),
        array("DIV" => "edit4", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_SHOP"), "ICON" => "gds_mobileapi_manual", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_SHOP")),
        array("DIV" => "edit5", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_REVIEW"), "ICON" => "gds_mobileapi_manual", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_REVIEW")),
        array("DIV" => "edit6", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_MARK"), "ICON" => "gds_mobileapi_manual", "TITLE" => GetMessage("GDS_MOBILEAPI_H_MANUAL_MARK")),
        array("DIV" => "edit7", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_TRANSACTION"), "ICON" => "gds_mobileapi_manual", "TITLE" => GetMessage("GDS_MOBILEAPI_H_MANUAL_TRANSACTION")),
        array("DIV" => "edit8", "TAB" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_ONESIGNAL"), "ICON" => "gds_transactions_settings", "TITLE" => GetMessage("GDS_MOBILEAPI_TAB_MANUAL_ONESIGNAL")),
        array("DIV" => "edit9", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "translate_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs);
	
	$tabControl->Begin();?>
	<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?=LANGUAGE_ID?>">
	<?bitrix_sessid_post();?>
	<?$tabControl->BeginNextTab();
	foreach($arOptions as $Option) {
		$val = COption::GetOptionString($module_id, $Option[0]);
		$type = $Option[3];?>
		<tr>
			<td class="settings"><?
				if ($type[0]=="checkbox") {
					echo "<label for=\"".htmlspecialcharsbx($Option[0])."\">".$Option[1]."</label>";
				} else {
					echo $Option[1];
				}
			?></td>
			<td class="settings">
				<?if($type[0]=="checkbox"):?>
					<input type="checkbox" name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
				<?elseif($type[0]=="text"):?>
					<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($Option[0])?>">
				<?elseif($type[0]=="textarea"):?>
					<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($Option[0])?>"><?echo htmlspecialcharsbx($val)?></textarea>
				<?elseif($type[0]=="selectbox"):?>
					<select name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>">
						<?foreach($Option[4] as $v => $k)
						{
							?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
						}
						?>
					</select>
				<?endif?>
			</td>
		</tr>
	<?
	}
$tabControl->BeginNextTab();?>
<tr>
	<td valign="top" colspan="2">
		<div>
			<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_DATA_FIELDS_LINK')?></span>
			<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_DATA_FIELDS_FULL').'</pre>'?></div>
		</div><div>
			<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_DATA_HASH_LINK')?></span>
			<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_DATA_HASH_FULL').'</pre>'?></div>
		</div><div>
			<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_DATA_ANSWER_LINK')?></span>
			<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_DATA_ANSWER_FULL').'</pre>'?></div>
		</div><div>
			<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_DATA_ERRORS_LINK')?></span>
			<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_DATA_ERRORS_FULL').'</pre>'?></div>
		</div>
	</td>
</tr>
<?$tabControl->BeginNextTab();?>  
    <tr>
        <td valign="top" colspan="2">
			<?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_USER_AUTH_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_USER_AUTH_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_USER_REGISTER_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_USER_REGISTER_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_USER_FORGOT_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_USER_FORGOT_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_USER_GET_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_USER_GET_FULL').'</pre>'?></div>
			</div>
		</td>
    </tr>
<?$tabControl->BeginNextTab();?>
	    <tr>
        <td valign="top" colspan="2">
			<?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_AUTH_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_AUTH_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_REGISTER_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_REGISTER_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_FASTREGISTER_LINK')?></span>
				<br><span><?=GetMessage('GDS_MOBILEAPI_SHOP_FASTREGISTER_SHORT')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_FASTREGISTER_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_NEWPASS_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_NEWPASS_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_DESCRIBE_LINK')?></span>
				<br><span><?=GetMessage('GDS_MOBILEAPI_SHOP_DESCRIBE_SHORT')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_DESCRIBE_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_SHOP_GETLIST_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_SHOP_GETLIST_FULL').'</pre>'?></div>
			</div>
		</td>
    </tr>
<?$tabControl->BeginNextTab();?>
	<tr>
		<td valign="top" colspan="2">
			<?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_REVIEW_ADD_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_REVIEW_ADD_FULL').'</pre>'?></div>
			</div>
		</td>
	</tr>
<?$tabControl->BeginNextTab();?>
	<tr>
        <td valign="top" colspan="2">
			<?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_MARK_ADD_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_MARK_ADD_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_MARK_SHOPLIST_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_MARK_SHOPLIST_FULL').'</pre>'?></div>
			</div>
		</td>
    </tr>
<?$tabControl->BeginNextTab();?>
	<tr>
        <td valign="top" colspan="2">
			<?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_TRANSACTION_ADD_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_TRANSACTION_ADD_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_TRANSACTION_WRITEOFF_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_TRANSACTION_WRITEOFF_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_TRANSACTION_BALANCE_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_TRANSACTION_BALANCE_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_TRANSACTION_GETLIST_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_TRANSACTION_GETLIST_FULL').'</pre>'?></div>
			</div>
		</td>
    </tr>
<?$tabControl->BeginNextTab();?>
	<tr><td colspan="2"><?=GetMessage("GDS_MOBILEAPI_TAB_SETTINGS")?>:</td></tr>
	<tr>
		<td class="os-settings" width="300"><?=GetMessage("GDS_MOBILEAPI_OS_RESTKEY")?></td>
		<td class="os-settings">
			<input type="text" size="48" value="<?=htmlspecialcharsbx(COption::GetOptionString($module_id, "OS_RESTKEY"))?>" name="<?=htmlspecialcharsbx("OS_RESTKEY")?>">
		</td>
	</tr>
	<tr>
		<td class="os-settings" width="300"><?=GetMessage("GDS_MOBILEAPI_OS_ADMINID")?></td>
		<td class="os-settings">
			<input type="text" size="36" value="<?=htmlspecialcharsbx(COption::GetOptionString($module_id, "OS_ADMINID"))?>" name="<?=htmlspecialcharsbx("OS_ADMINID")?>">
		</td>
	</tr>
	<tr>
        <td valign="top" colspan="2">
			<br><br><?=GetMessage('GDS_MOBILEAPI_MANUAL_HEAD')?><br><br>
			<div>
				<span class="method-link"><?=GetMessage('GDS_MOBILEAPI_ONESIGNAL_SUBSCRIBE_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_ONESIGNAL_SUBSCRIBE_FULL').'</pre>'?></div>
			</div><div>
				<br><br><span class="method-link"><?=GetMessage('GDS_MOBILEAPI_ONESIGNAL_MESSAGE_LINK')?></span>
				<div class="method-text"><?='<pre>'.GetMessage('GDS_MOBILEAPI_ONESIGNAL_MESSAGE_FULL').'</pre>'?></div>
			</div>
		</td>
    </tr>
<?$tabControl->BeginNextTab();?>
    <?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");?>
    <?$tabControl->Buttons();?>
    <script language="JavaScript">
        function RestoreDefaults()
        {
            if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
                window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
        }
    </script>

    <input type="submit" <?if ($GDS_MOBILEAPI_RIGHT<"W") echo "disabled" ?> name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
    <input type="hidden" name="Update" value="Y">
    <input type="reset" name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
    <input type="button" <?if ($GDS_MOBILEAPI_RIGHT<"W") echo "disabled" ?> title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="RestoreDefaults();" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
    <?$tabControl->End();?>
    </form>
<?endif;?>
