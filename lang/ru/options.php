<?
$MESS['GDS_MOBILEAPI_TAB_SETTINGS'] = "���������";
$MESS['GDS_MOBILEAPI_TAB_MANUAL_DATA'] = '������';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_USER'] = 'User';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_SHOP'] = 'Shop';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_REVIEW'] = 'Review';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_MARK'] = 'Mark';
$MESS['GDS_MOBILEAPI_H_MANUAL_MARK'] = '����� ��� ����';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_TRANSACTION'] = 'Transaction';
$MESS['GDS_MOBILEAPI_H_MANUAL_TRANSACTION'] = '����������';
$MESS['GDS_MOBILEAPI_TAB_MANUAL_ONESIGNAL'] = 'OneSignal';

$MESS['GDS_MOBILEAPI_SECRET_WORD'] = "��������� �����";
$MESS['GDS_MOBILEAPI_HASH_PERIOD'] = "������ �������� �����, ���";
$MESS['GDS_MOBILEAPI_TIME_PERIOD'] = "���������� ���������� TIME, ����";
$MESS['GDS_MOBILEAPI_OS_RESTKEY'] = "REST API key";
$MESS['GDS_MOBILEAPI_OS_ADMINID'] = "App ID";
$MESS['GDS_MOBILEAPI_MANUAL_HEAD'] = "������ ������:";

$MESS['GDS_MOBILEAPI_DATA_FIELDS_LINK'] = "��������� ���� � ��������";
$MESS['GDS_MOBILEAPI_DATA_FIELDS_FULL'] = "
ADDRESS - string - ���������� ����� ��������
AMOUNT - integer - ���������� ������ ��� ���������� ��� ��������
APP - string - ���������� ��� ����������, ������������� push-�����������
CARD - string - ����� �����
CITY - string - �����
COUNTRY - string - ������
DETAIL_TEXT - text - ��������� �������� ����� ������
DOC_DATE - ���� � ����� � ������� ����� (06.04.2016 15:39:52)
DOC_NUMBER - string - ����� ��������� ��������� ����������
DOC_TYPE - ��� ��������� ��������� ����������: \"order\", \"check\" ��� \"social\"
EMAIL - string - ����� ����������� �����
FULLNAME - string - ������ �������� ��������
ID - integer/string - ����� �������������, ������� �� ������
LATITUDE - float - �������������� ������ (51.54056)
LOGIN - string - �����
LOGOIMAGELINK - file - ����������� �������� (�� ��������� � ������������ ����)
LONGITUDE - float - �������������� ������� (46.00861)
MARK - string - ����� ����� ��� �����
NAME - string - ��� ��� ���������
OS - string - ���������� ��� �� ���������� ������������
PASS - string - ������������� ������
PHONE - string - ����� ��������
PLAYER - string - player ID ��� API OneSignal
PREVIEW_TEXT - text - ������� �������� ����� ������
PROVIDER - string - ���������� ��� ���������� push-�����������
RATING - integer - ������ ���������� ��������
SECTION - string - �������� ������� ��
SELECT - string - ������ ����� ������������ ����� ������� (EMAIL,LOGIN,NAME,LAST_NAME,UF_NUMBER_CART)
SENDON - timestamp - �����, ����� �������� ����� ��������� �����������
SHOPCODE - string - ��������� ��� ��������
SHOPURL - string - ����� ����� ��������
SURNAME - string - �������
TEXT - string - ����� ������ ��� ���������
TIME - float - ��������� ������ ������� microtime(true) (1476097050.1015)
TYPE - string - ���������� ��� ���� ���������
HASH - string - md5-��� �� ����� �������, ������� � ���������� �����";
$MESS['GDS_MOBILEAPI_DATA_HASH_LINK'] = "��������� ����";
$MESS['GDS_MOBILEAPI_DATA_HASH_FULL'] = "
� ������ ������� ����������� ��������� 2 ����: TIME � HASH.
��� �������������� �� ��������� md5 �� ���� ����� ������� � ���������� �������
(���������� TIME), ������ (���� ����� � ������ ������) � ���������� ����� �� �������� ������.

������ ��������� ���� HASH:
LOGIN = gamma
EMAIL = gamma@bohemia.com
TIME = 1475587288
�������������� ������ = zzz123
��������� ����� = garden
HASH = md5(\"gamma@bohemia.comgamma1475587288zzz123garden\")";
$MESS['GDS_MOBILEAPI_DATA_ANSWER_LINK'] = "�����";
$MESS['GDS_MOBILEAPI_DATA_ANSWER_FULL'] = "
status - ������ \"error\" ��� \"ok\"
message - ���������
error_code - ���� ������ \"error\", �� ��������� ��� ������, ��������: \"INVALID_HASH\"";
$MESS['GDS_MOBILEAPI_DATA_ERRORS_LINK'] = "����� ������";
$MESS['GDS_MOBILEAPI_DATA_ERRORS_FULL'] = "
{\"status\":\"error\",\"message\":\"�� ������� ������� �����: <��� ������>\",\"error_code\":\"NOT_METHOD\"} - �������� �������������� ��� ������
{\"status\":\"error\",\"message\":\"������������ �� ������\",\"error_code\":\"ERROR_GET_USER\"} - ������� �������������� ID ������������
{\"status\":\"error\",\"message\":\"�������� ��� ��������\",\"error_code\":\"ERROR_SHOP_CODE\"} - ������� �������������� ���������� ��� ��������
{\"status\":\"error\",\"message\":\"����� ������� ������� ������ ���������� �� ������� �������\",\"error_code\":\"INVALID_TIME\"} - ��������� �������� ���������� �� �������
{\"status\":\"error\",\"message\":\"�������� ���\",\"error_code\":\"INVALID_HASH\"} - ��� �� ������� �� ��������� � �����������
{\"status\":\"error\",\"message\":\"������ �� ����� ���� ��������� � ��������� �����\",\"error_code\":\"ERROR_HASH_CHECK\"} - ������ � ����� ����� ��� �������������
{\"status\":\"error\",\"message\":\"�������� ������ �����\",\"error_code\":\"INVALID_FORMAT_EMAIL\"} - ������������ ������ email-������
{\"status\":\"error\",\"message\":\"��� ����������: EMAIL, FULLNAME\",\"error_code\":\"NOT_CODE\"} - ���������� ������������ ��������
{\"status\":\"error\",\"message\":\"����� ����� �� ����������\",\"error_code\":\"ERROR_USER_CARD\"} - ������� �������������� ����� �����
{\"status\":\"error\",\"message\":\"����� ����� �������� � ������� email-������\",\"error_code\":\"ERROR_USER_EMAIL\"} - ����� ����� � ���� �������� � ������� email-������";

$MESS['GDS_MOBILEAPI_USER_AUTH_LINK'] = "/mobile_api/user/auth/ - ����������� ������������";
$MESS['GDS_MOBILEAPI_USER_AUTH_FULL'] = "
����:
LOGIN - string - ����� ������������

������ �������:
/mobile_api/user/auth/?LOGIN=lda@greenkevich.by&TIME=1475582476&HASH=0dfaab19424bf4a6d1e359d2525ba59e

���������:
{
\"status\":\"ok\",
\"message\":\"<����� ����� ������������>\",
\"error_code\":\"0\"
}

���� ����� � ������ ����������, �� ����� ����� ������:
{
\"status\":\"error\",
\"message\":\"� ������������ ��� �����\",
\"error_code\":\"ERROR_NOT_CARD\"
}";
$MESS['GDS_MOBILEAPI_USER_REGISTER_LINK'] = "/mobile_api/user/register/ - ����������� ������������";
$MESS['GDS_MOBILEAPI_USER_REGISTER_FULL'] = "
����:
EMAIL - string - ����� ����������� �����
NAME - string - ���
SURNAME - string - �������

������ �������:
/mobile_api/user/register/?EMAIL=lda23000@greenkevich.by&NAME=Dmitry&SURNAME=Lapshin&TIME=1475583036&HASH=bc15a03b3ce3d873989a44c3d337d2db

���������:
{
\"status\":\"ok\",
\"message\":\"<����� �����, �������� ������������ ��� �����������>\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������ ��� �����������: ������������ � ����� e-mail (test@yandex.ru) ��� ����������.\",
\"error_code\":\"ERROR_REGISTRATION\"
}";
$MESS['GDS_MOBILEAPI_USER_FORGOT_LINK'] = "/mobile_api/user/forgot/ - �������������� ������";
$MESS['GDS_MOBILEAPI_USER_FORGOT_FULL'] = "
����:
EMAIL - string - ����� ����������� �����

������ �������:
/mobile_api/user/forgot/?EMAIL=lda@greenkevich.by&TIME=1475506205&HASH=53f7e1c48d768a050fbb9463b33eed26

���������:
{
\"status\":\"ok\",
\"message\":\"����� ������ ������ �� �����\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"��������� ������������ �����\",
\"error_code\":\"INCORRECT_EMAIL\"
}";
$MESS['GDS_MOBILEAPI_USER_GET_LINK'] = "/mobile_api/user/get/ - ��������� ������ ������������";
$MESS['GDS_MOBILEAPI_USER_GET_FULL'] = "
����:
LOGIN - string - �����
SELECT - string - ������ ����� ������������ ����� ������� (EMAIL,LOGIN,NAME,LAST_NAME,UF_NUMBER_CART)

������ �������:
/mobile_api/user/get/?LOGIN=lda@greenkevich.by&SELECT=EMAIL,LOGIN,NAME,LAST_NAME,UF_NUMBER_CART&TIME=1475585210&HASH=82bfdfd81088dde6492d52821e0d7fad

���������:
{
\"status\":\"ok\",
\"message\":{\"EMAIL\":\"lda@greenkevich.by\",\"LOGIN\":\"lda@greenkevich.by\",\"NAME\":\"�������\",\"LAST_NAME\":\"������\",\"UF_NUMBER_CART\":\"0000012\"},
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������������ �� ������\",
\"error_code\":\"ERROR_GET_USER\"
}";

$MESS['GDS_MOBILEAPI_SHOP_AUTH_LINK'] = "/mobile_api/shop/auth/ - ����������� ��������";
$MESS['GDS_MOBILEAPI_SHOP_AUTH_FULL'] = "
����:
LOGIN - string - �����

������ ������a:
/mobile_api/shop/auth/?LOGIN=gamma&TIME=1475581150&HASH=37b35faffd020eb93e405837332b86d3

���������:
{
\"status\":\"ok\",
\"message\":\"true\",
\"error_code\":\"0\"
}";
$MESS['GDS_MOBILEAPI_SHOP_REGISTER_LINK'] = "/mobile_api/shop/register/ - ����������� ��������";
$MESS['GDS_MOBILEAPI_SHOP_REGISTER_FULL'] = "
����:
EMAIL - string - email-����� ������������, ��������������� �������
LOGIN - string - ���������� ��� ��������������� ��������
NAME - string - �������� ��������������� ��������
PASS - string - ������ ��������������� �������� (�������������)

������ ������a:
/mobile_api/shop/register/?EMAIL=lda@greenkevich.by&LOGIN=delta&NAME=UAC Delta&PASS=Betruger&TIME=1475494090&HASH=2c016fd3de319f44b63b319d106ed2a7

���������:
{
\"status\":\"ok\",
\"message\":\"������� ������� ���������������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������ ��� �����������: ������� � ����� ���������� ����� ��� ����������.\",
\"error_code\":\"ERROR_REGISTRATION\"
}";
$MESS['GDS_MOBILEAPI_SHOP_FASTREGISTER_LINK'] = "/mobile_api/shop/fastregister/ - �������������� ����������� ��������";
$MESS['GDS_MOBILEAPI_SHOP_FASTREGISTER_SHORT'] = "����� � ������ � ���� ������ ������������ �������������.";
$MESS['GDS_MOBILEAPI_SHOP_FASTREGISTER_FULL'] = "
����:
EMAIL - string - email-����� ������������, ��������������� �������
NAME - string - �������� ��������������� ��������

������ ������a:
/mobile_api/shop/fastregister/?EMAIL=lda@greenkevich.by&NAME=��������� ���&TIME=1475653338&HASH=2a5feee7ec9c27d04c94756c791b009c

���������:
{
\"status\":\"ok\",
\"message\":{
\"login\":\"seryeznyy_sem2\",
\"password\":\"YoiS2rHKjR\"
},
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������ ��� �����������: <����� ������>\",
\"error_code\":\"ERROR_REGISTRATION\"
}";
$MESS['GDS_MOBILEAPI_SHOP_NEWPASS_LINK'] = "/mobile_api/shop/newpass/ - �������� ������ ������ ��� ��������";
$MESS['GDS_MOBILEAPI_SHOP_NEWPASS_FULL'] = "
����:
EMAIL - string - email-����� ������������, ����������������� �������
SHOPCODE - string - ��������� ��� ��������, ��� �������� �������� ������

������ ������a:
/mobile_api/shop/newpass/?EMAIL=lda@greenkevich.by&SHOPCODE=delta&TIME=1475494090&HASH=d034b86068e87833a3af6e544f2f5621

���������:
{
\"status\":\"ok\",
\"message\":\"����� ������ ������ �� �����\",
\"error_code\":\"0\"
}

���� ������������ �� �������� ��������������� ��������:
{
\"status\":\"error\",
\"message\":\"������������ �� �������� ��������������� ������� ��������\",
\"error_code\":\"ERROR_SHOP_AUTH\"
}
���� �� ������� ��������� ������ � ����� �������:
{
\"status\":\"error\",
\"message\":\"�� ������� ��������� ������\",
\"error_code\":\"ERROR_SENDMAIL\"
}";
$MESS['GDS_MOBILEAPI_SHOP_DESCRIBE_LINK'] = "/mobile_api/shop/describe/ - �������� ��������";
$MESS['GDS_MOBILEAPI_SHOP_DESCRIBE_SHORT'] = "����� ��������� ������ POST-�������, ��� ��� ��� ��� ����, ������� ��������, �������� �������������.";
$MESS['GDS_MOBILEAPI_SHOP_DESCRIBE_FULL'] = "
����, ������� �� ����� ���� �������:
NAME - string - ������� �������� �������� (�������� �������� ��)
SECTION - string - �������� ������� ��
SHOPCODE - string - ��������� ��� ��������

����, ������� ����� ���� ������� (��� ���� ������ �������� ������������� �� ������):
ADDRESS - string - ���������� ����� ��������
CITY - string - �����
COUNTRY - string - ������
DETAIL_TEXT - text - ��������� �������� ����� ������
EMAIL - string - ����� ����������� �����
FULLNAME - string - ������ �������� ��������
LATITUDE - double - �������������� ������ (51.54056)
LOGOIMAGELINK - file - ����������� �������� �������� (�� ��������� � ������������ ����)
LONGITUDE - double - �������������� ������� (46.00861)
PHONE - string - ����� ��������
PREVIEW_TEXT - text - ������� �������� ����� ������
SHOPURL - string - ����� ����� ��������

ID - �������������� ����
� ������, ���� ID �� ��������� ��� �� ��������� �� � ����� ������������ ��������� �� \"����� ������\",
�������� ����� ����� ������. ���� ID ���������� � ������, ���������� �������� ��������������� ����� ������.

����������:
{
\"status\":\"ok\",
\"message\":\"�������� �������� ������� ��������\",
\"error_code\":\"0\"
}
{
\"status\":\"ok\",
\"message\":\"�������� �������� ������� ���������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������ ��� ���������� ������: <����� ������>\",
\"error_code\":\"ERROR_UPDATE\"
}
���� � ������� �� �������� ��� ����:
{
\"status\":\"error\",
\"message\":\"����������� ���� ��������: LATITUDE, LOGOIMAGELINK, LONGITUDE\",
\"error_code\":\"ERROR_SHOP_FIELDS\"
}
���� �������� �������������� ��� �������:
{
\"status\":\"error\",
\"message\":\"������ �� ����������\",
\"error_code\":\"ERROR_SHOP_SECTION\"
}";
$MESS['GDS_MOBILEAPI_SHOP_GETLIST_LINK'] = "/mobile_api/shop/getlist/ - ����� ������ ��������� �� ������������";
$MESS['GDS_MOBILEAPI_SHOP_GETLIST_FULL'] = "
����:
EMAIL - string - email-����� ������������, ����������������� ��������

������ ������a:
/mobile_api/shop/getlist/?EMAIL=lda@greenkevich.by&TIME=1475494090&HASH=180ea10944c20d90c251f7e4220d2efb

���������:
{
\"status\":\"ok\",
\"message\":[
{
\"CODE\":\"beta\",
\"NAME\":\"��� ����\",
\"PASSWORD\":\"O4z9Gr7y\"
},
{
\"CODE\":\"gamma\",
\"NAME\":\"��� �����\"
\"PASSWORD\":\"gamma\"
}
],
\"error_code\":\"0\"
}";

$MESS['GDS_MOBILEAPI_REVIEW_ADD_LINK'] = "/mobile_api/review/add - ���������� ������";
$MESS['GDS_MOBILEAPI_REVIEW_ADD_FULL'] = "
����:
DOC_DATE - ���� � ����� ������ � ������� ����� (06.04.2016 15:39:52)
EMAIL - string - ����� ����������� �����
RATING - integer - ������ ���������� ��������
SHOPCODE - string - ��������� ��� ��������
TEXT - string - ����� ������ ����������

������ ������a:
/mobile_api/review/add/?DOC_DATE=04.10.2016 13:37:00&EMAIL=lda@greenkevich.by&RATING=10&SHOPCODE=gamma&TEXT=Nice posture!&TIME=1475578835&HASH=4bd033573876f486b5fef2799cf0618a

���������:
{
\"status\":\"ok\",
\"message\":\"����� ������� ��������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"����� �� ����� ���� ��������\",
\"error_code\":\"ERROR_REVIEW_ADD\"
}";

$MESS['GDS_MOBILEAPI_MARK_ADD_LINK'] = "/mobile_api/mark/add - ���������� �����";
$MESS['GDS_MOBILEAPI_MARK_ADD_FULL'] = "
����:
CARD - string - ����� �����
MARK - string - ������������� �����
SHOPCODE - string - ��������� ��� ��������

������ ������a:
/mobile_api/mark/add/?CARD=0000014&MARK=�������&SHOPCODE=mir_uvlecheniy&TIME=1475494090&HASH=439439b020ed6e51a937e11dda2fd141

���������:
{
\"status\":\"ok\",
\"message\":\"����� ���������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"����� �� ���������\",
\"error_code\":\"ERROR_MARK_ADD\"
}";
$MESS['GDS_MOBILEAPI_MARK_SHOPLIST_LINK'] = "/mobile_api/mark/shoplist - ������ ����� ����������� ��������";
$MESS['GDS_MOBILEAPI_MARK_SHOPLIST_FULL'] = "
����:
SHOPCODE - string - ��������� ��� ��������
EMAIL - string - ����� ����������� ����� ���������� (��������������, ��� ������ ����� ������ ����� ���������� ��������)

������� ������a:
/mobile_api/mark/shoplist/?SHOPCODE=alfa&TIME=1476343779.6514&HASH=36f7355d971c404f2de06776f89f944c
/mobile_api/mark/shoplist/?EMAIL=goldenplan@yandex.ru&SHOPCODE=alfa&TIME=1476343779.6514&HASH=e7ff368ea7bf3f57587bed5f969d4d79

���������:
{
\"status\":\"ok\",
\"message\":[\"�������\",\"������\"],
\"error_code\":\"0\"
}";

$MESS['GDS_MOBILEAPI_TRANSACTION_ADD_LINK'] = "/mobile_api/transaction/add - ���������� ������";
$MESS['GDS_MOBILEAPI_TRANSACTION_ADD_FULL'] = "
����:
AMOUNT - integer - ���������� ������ ��� ����������
CARD - string - ����� �����
DOC_DATE - ���� ��������� ��������� ���������� (06.04.2016 15:39:52)
DOC_NUMBER - string - ����� ��������� ��������� ����������
DOC_TYPE - ��� ��������� ��������� ����������: \"order\", \"check\" ��� \"social\"
EMAIL - string - ����� ����������� ����� ����������
SHOPCODE - string - ��������� ��� ��������

������ ������a:
/mobile_api/transaction/add/?AMOUNT=100&CARD=0000014&DOC_DATE=03.10.2016%2013:37:00&DOC_NUMBER=UT-2004&DOC_TYPE=check&EMAIL=lda@greenkevich.by&SHOPCODE=delta&TIME=1475494090&HASH=1fa788d0075f286f756680ca6a5c027e

���������:
{
\"status\":\"ok\",
\"message\":\"���������� ������� �������. ����� ���������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"��������� ������. �� ������� ������� ����������\",
\"error_code\":\"ERROR_TRANSACTION_ADD\"
}";
$MESS['GDS_MOBILEAPI_TRANSACTION_WRITEOFF_LINK'] = "/mobile_api/transaction/writeoff - �������� ������";
$MESS['GDS_MOBILEAPI_TRANSACTION_WRITEOFF_FULL'] = "
����:
AMOUNT - integer - ���������� ������ ��� ��������
CARD - string - ����� �����
DOC_DATE - ���� ��������� ��������� ���������� (06.04.2016 15:39:52)
DOC_NUMBER - string - ����� ��������� ��������� ����������
DOC_TYPE - ��� ��������� ��������� ����������: \"order\", \"check\" ��� \"social\"
EMAIL - string - ����� ����������� ����� ����������
SHOPCODE - string - ��������� ��� ��������

������ ������a:
/mobile_api/transaction/writeoff/?AMOUNT=100&CARD=0000014&DOC_DATE=03.10.2016%2013:37:00&DOC_NUMBER=UT-2004&DOC_TYPE=check&EMAIL=lda@greenkevich.by&SHOPCODE=delta&TIME=1475500978&HASH=13debab6c88d8814a280ea7b72c45298

���������:
{
\"status\":\"ok\",
\"message\":\"���������� ������� �������. ����� �������\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"��������� ������. �� ������� ������� ����������\",
\"error_code\":\"ERROR_TRANSACTION_ADD\"
}";
$MESS['GDS_MOBILEAPI_TRANSACTION_BALANCE_LINK'] = "/mobile_api/transaction/balance - ����� ������� ������";
$MESS['GDS_MOBILEAPI_TRANSACTION_BALANCE_FULL'] = "
����:
CARD - string - ����� �����
EMAIL - string - ����� ����������� ����� ����������
SHOPCODE - string - ��������� ��� ��������

������ ������a:
/mobile_api/transaction/balance/?CARD=0000014&EMAIL=lda@greenkevich.by&SHOPCODE=delta&TIME=1475502364&HASH=e7c7f89a42fb61f98f09f51756d6b991

���������:
{
\"status\":\"ok\",
\"message\":\"100\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"���������� �� �������\",
\"error_code\":\"ERROR_TRANSACTION_GET\"
}";
$MESS['GDS_MOBILEAPI_TRANSACTION_GETLIST_LINK'] = "/mobile_api/transaction/getlist - ����� ������ ����������";
$MESS['GDS_MOBILEAPI_TRANSACTION_GETLIST_FULL'] = "
����:
CARD - string - ����� �����
EMAIL - string - ����� ����������� ����� ����������
SHOPCODE - string - ��������� ��� �������� (��������������, ��� ������ ���������� ������ �� ����������� ��������)

������� ������a:
/mobile_api/transaction/getlist/?CARD=0000014&EMAIL=lda@greenkevich.by&SHOPCODE=delta&TIME=1475502520&HASH=f2426d40f4b14a0e8bf975969e4b61a4
/mobile_api/transaction/getlist/?CARD=0000014&EMAIL=lda@greenkevich.by&TIME=1475502520&HASH=32374a8c7d11d9ef44b9aba7aaefa6be

������ ����������:
{
\"status\":\"ok\",
\"message\":[
{
\"ID\":\"135\",
\"ACTIVE\":\"N\",
\"TRANSACTION_DATE\":\"16.08.2016 17:20:08\",
\"SHOP\":\"��� ��������� [mir_uvlecheniy]\",
\"TRANSACTION_TYPE\":\"��������\",
\"AMOUNT\":\"-500\",
\"NUMBER_CART\":\"0000010\",
\"EMAIL\":\"pavel@dev-bitrix.by\",
\"DOC_TYPE\":\"�����\",
\"DOC_NUMBER\":\"��-1234\",
\"DOC_DATE\":\"06.04.2016 05:39:52\"
},
{
\"ID\":\"319\",
\"ACTIVE\":\"Y\",
\"TRANSACTION_DATE\":\"22.08.2016 14:00:00\",
\"SHOP\":\"��� ��������� [mir_uvlecheniy]\",
\"TRANSACTION_TYPE\":\"����������\",
\"AMOUNT\":\"500\",
\"NUMBER_CART\":\"0000010\",
\"EMAIL\":\"pavel@dev-bitrix.by\",
\"DOC_TYPE\":\"�����\",
\"DOC_NUMBER\":\"��-1234\",
\"DOC_DATE\":\"06.04.2016 05:39:52\"
}
],
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"���������� �� �������\",
\"error_code\":\"ERROR_TRANSACTION_GET\"
}";

$MESS['GDS_MOBILEAPI_ONESIGNAL_SUBSCRIBE_LINK'] = "/mobile_api/onesignal/subscribe - ����������� ����������";
$MESS['GDS_MOBILEAPI_ONESIGNAL_SUBSCRIBE_FULL'] = "
������������ ����:
APP - string - ���������� ��� ����������, ������������� push-�����������
OS - string - ���������� ��� �� ���������� ����������
PLAYER - string - player ID ���������� ��� API OneSignal
PROVIDER - string - ���������� ��� ���������� push-�����������

�������������� ����:
EMAIL - string - ����� ����������� ����� ����������
NAME - string - ���������� ��� ������ ��������

������ ������a:
/mobile_api/onesignal/subscribe/?APP=verycard&EMAIL=pavel@dev-bitrix.by&NAME=gamma&OS=ios&PLAYER=11111111-2cc0-4a03-9a0a-cdbd36aa3f6a&PROVIDER=onesignal&TIME=1476107891.8309&HASH=695197483c8892d0d9f54a60dd7485c3

���������:
- ���� ��������� ��� ���������� � ������ �������� �� ��������:
{
\"status\":\"ok\",
\"message\":\"��������� ��� ����������\",
\"error_code\":\"0\"
}
- ���� ��������������� ����� ���������, �� ������ �������� �� ��������:
{
\"status\":\"ok\",
\"message\":\"��������� ������� ���������������\",
\"error_code\":\"0\"
}
- ���� �������� ������ ��������:
{
\"status\":\"ok\",
\"message\":\"������������ �������� �� ��������� ��������\",
\"error_code\":\"0\"
}

������ ��� ����������� ������ ����������:
{
\"status\":\"error\",
\"message\":\"������ ��� �����������: <����� ������>\",
\"error_code\":\"ERROR_REGISTRATION\"
}
������ ��� ���������� ������ ����������:
{
\"status\":\"error\",
\"message\":\"������ ��� ���������� ������: <����� ������>\",
\"error_code\":\"ERROR_UPDATE\"
}";
$MESS['GDS_MOBILEAPI_ONESIGNAL_MESSAGE_LINK'] = "/mobile_api/onesignal/message - ����������� ���������";
$MESS['GDS_MOBILEAPI_ONESIGNAL_MESSAGE_FULL'] = "
������������ ����:
NAME - string - ��������� ���������
SENDON - timestamp - �����, ����� �������� ����� ��������� ����������� (1476188955)
TEXT - string - ����� ���������
TYPE - string - ���������� ��� ���� ���������

����� ����, ������ �������������� ���� �� ���� �� ��������� �����:
ID - string - ���������� ��� ������ ��������
PLAYER - string - Player ID ��� �������� ������ ����������
� ������, ���� � ������� ��������� ��� ����, �������� ��� ������ �� Player ID.

������ ������a:
/mobile_api/onesignal/message/?ID=gamma&NAME=��� ����� �����������!&SENDON=1476188955&TEXT=����������� ������ ��������� � ������ ������ ������ ������������ ��� ������������ �������� ����������� ����������.&TIME=1476180955.5726&TYPE=simple&HASH=c797b59a1f45cfbfbd9255045bf5cb3f

���������:
{
\"status\":\"ok\",
\"message\":\"<ID ���������� �������� � �� \"���� ���������\">\",
\"error_code\":\"0\"
}

��� ������:
{
\"status\":\"error\",
\"message\":\"������ ��� �����������: <����� ������>\",
\"error_code\":\"ERROR_REGISTRATION\"
}
���� ������� �������������� ���������� ��� ���� ���������:
{
\"status\":\"error\",
\"message\":\"�������� ��� ���������\",
\"error_code\":\"ERROR_OS_MESSTYPE\"
}
���� ����������� � ���� ID, � ���� PLAYER:
{
\"status\":\"error\",
\"message\":\"�� ������� ������������� ���������� ��� ������\",
\"error_code\":\"ERROR_OS_NOID\"
}";
